import {createRouter, createWebHistory} from 'vue-router'

import Home from '@/views/Home.vue'
import ProductDetail from "@/views/ProductDetail.vue";

export const router = createRouter({
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/product/:productId',
            name: 'product-detail',
            component: ProductDetail
        }
    ],
    history: createWebHistory(),
    mode: 'history'
})
