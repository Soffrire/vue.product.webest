import axios from "axios";

const Products = {
    state: {
        products: [],
        product: null
    },
    actions: {
        fetchProducts({commit}) {
            axios.get('https://jsonplaceholder.typicode.com/posts?_limit=5')
                .then(response => commit('setProducts', response.data))
                .catch(error => console.log(error))
        },
        fetchProductById({commit}, productId) {
            return axios.get(`https://jsonplaceholder.typicode.com/posts?id=${productId}`)
                .then(response => commit('setProduct', response.data))
        }
    },
    mutations: {
        setProducts(state, products) {
            state.products = products
        },
        setProduct(state, product) {
            state.product = product
        }
    },
    getters: {
        getProducts: state => state.products,
        getProductById: state => id => {
            return state.products.find(product => product.id === +id)
        }
    }
}

export default Products
