import {createStore} from 'vuex'
import Products from '@/store/modules/products'

export const store = createStore({
    modules: {
        Products
    }
})
